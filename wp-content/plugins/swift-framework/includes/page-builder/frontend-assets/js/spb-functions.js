/*global jQuery,google */

var SPB = SPB || {};

(function(){

	// USE STRICT
	"use strict";

	/////////////////////////////////////////////
	// ANIMATED HEADLINE
	/////////////////////////////////////////////

 	SPB.animatedHeadline = {
		init: function () {
			var animatedHeadlines = jQuery('.spb-animated-headline'),
				animationDelay = 2500;

			animatedHeadlines.each( function() {
				var headline = jQuery(this).find('.sf-headline');

				setTimeout( function() {
					SPB.animatedHeadline.animateHeadline( headline );
				}, animationDelay);
			});

			// Single letter animation
			SPB.animatedHeadline.singleLetters( jQuery('.sf-headline.letters').find('b') );
		},
		singleLetters: function ( $words ) {
			$words.each( function() {
				var word = jQuery(this),
					letters = word.text().split(''),
					selected = word.hasClass('is-visible');

				for ( var i in letters ) {
					if ( word.parents('.rotate-2').length > 0 ) letters[i] = '<em>' + letters[i] + '</em>';
					letters[i] = ( selected ) ? '<i class="in">' + letters[i] + '</i>': '<i>' + letters[i] + '</i>';
				}

			    var newLetters = letters.join('');
			    word.html( newLetters ).css( 'opacity', 1 );
			});
		},
		animateHeadline: function ( $headlines ) {
			var duration = 2500;

			$headlines.each( function() {
				var headline = jQuery(this);
				
				if ( headline.hasClass('loading-bar') ) {
					duration = 3800;
					var barAnimationDelay = 3800,
						barWaiting = barAnimationDelay - 3000;
					setTimeout( function() {
						headline.find('.sf-words-wrapper').addClass('is-loading')
					}, barWaiting);
				} else if ( headline.hasClass('clip') ) {
					var spanWrapper = headline.find('.sf-words-wrapper'),
						newWidth = spanWrapper.width() + 10
					spanWrapper.css('width', newWidth);
				} else if ( !headline.hasClass('type') ) {
					//assign to .sf-words-wrapper the width of its longest word
					var words = headline.find('.sf-words-wrapper b'),
						width = 0;
					words.each( function() {
						var wordWidth = jQuery(this).width();
					    if (wordWidth > width) width = wordWidth;
					});
					headline.find('.sf-words-wrapper').css('width', width);
				};

				//trigger animation
				setTimeout( function() {
					SPB.animatedHeadline.hideWord( headline.find('.is-visible').eq(0) );
				}, duration);
			});
		},
		hideWord: function ( $word ) {
			var nextWord = SPB.animatedHeadline.takeNext( $word ),
				animationDelay = 2500,
				lettersDelay = 50,
				typeLettersDelay = 150,
				selectionDuration = 500,
				typeAnimationDelay = selectionDuration + 800,
				revealDuration = 600,
				revealAnimationDelay = 1500,
				barAnimationDelay = 3800,
				barWaiting = barAnimationDelay - 3000;

			if ( $word.parents('.sf-headline').hasClass('type') ) {
				var parentSpan = $word.parent('.sf-words-wrapper');
				parentSpan.addClass('selected').removeClass('waiting');	
				setTimeout( function() { 
					parentSpan.removeClass('selected'); 
					$word.removeClass('is-visible').addClass('is-hidden').children('i').removeClass('in').addClass('out');
				}, selectionDuration);
				setTimeout( function() {
					SPB.animatedHeadline.showWord( nextWord, typeLettersDelay )
				}, typeAnimationDelay);
			} else if ( $word.parents('.sf-headline').hasClass('letters') ) {
				var bool = ( $word.children('i').length >= nextWord.children('i').length ) ? true : false;
				SPB.animatedHeadline.hideLetter( $word.find('i').eq(0), $word, bool, lettersDelay );
				SPB.animatedHeadline.showLetter( nextWord.find('i').eq(0), nextWord, bool, lettersDelay );
			}  else if ( $word.parents('.sf-headline').hasClass('clip') ) {
				$word.parents('.sf-words-wrapper').animate({ width : '2px' }, revealDuration, function(){
					SPB.animatedHeadline.switchWord( $word, nextWord );
					SPB.animatedHeadline.showWord( nextWord );
				});
			} else if ( $word.parents('.sf-headline').hasClass('loading-bar') ) {
				$word.parents('.sf-words-wrapper').removeClass('is-loading');
				SPB.animatedHeadline.switchWord($word, nextWord);
				setTimeout( function() {
					SPB.animatedHeadline.hideWord( nextWord );
				}, barAnimationDelay);
				setTimeout( function() {
					$word.parents('.sf-words-wrapper').addClass('is-loading');
				}, barWaiting);
			} else {
				SPB.animatedHeadline.switchWord( $word, nextWord );
				setTimeout( function() {
					SPB.animatedHeadline.hideWord( nextWord )
				}, animationDelay);
			}
		},
		showWord: function ( $word, $duration ) {
			var revealDuration = 600,
				revealAnimationDelay = 1500;

			if ( $word.parents('.sf-headline').hasClass('type') ) {
				SPB.animatedHeadline.showLetter( $word.find('i').eq(0), $word, false, $duration );
				$word.addClass('is-visible').removeClass('is-hidden');
			} else if ( $word.parents('.sf-headline').hasClass('clip') ) {
				$word.parents('.sf-words-wrapper').animate({
					'width' : $word.width() + 10
				}, revealDuration, function() { 
					setTimeout( function() {
						SPB.animatedHeadline.hideWord( $word )
					}, revealAnimationDelay); 
				});
			}
		},
		hideLetter: function ( $letter, $word, $bool, $duration ) {
			var animationDelay = 2500;

			$letter.removeClass('in').addClass('out');
			
			if ( !$letter.is(':last-child') ) {
			 	setTimeout( function() {
			 		SPB.animatedHeadline.hideLetter( $letter.next(), $word, $bool, $duration );
			 	}, $duration);  
			} else if ( $bool ) { 
			 	setTimeout( function() {
			 		SPB.animatedHeadline.hideWord( SPB.animatedHeadline.takeNext( $word ) )
			 	}, animationDelay);
			}

			if ( $letter.is(':last-child') && jQuery('html').hasClass('no-csstransitions') ) {
				var nextWord = takeNext( $word );
				SPB.animatedHeadline.switchWord( $word, nextWord );
			} 
		},
		showLetter: function ( $letter, $word, $bool, $duration ) {
			var animationDelay = 2500;

			$letter.addClass('in').removeClass('out');
			
			if ( !$letter.is(':last-child') ) { 
				setTimeout( function() {
					SPB.animatedHeadline.showLetter( $letter.next(), $word, $bool, $duration );
				}, $duration ); 
			} else { 
				if ( $word.parents('.sf-headline').hasClass('type') ) {
					setTimeout( function() {
						$word.parents('.sf-words-wrapper').addClass('waiting');
					}, 200);
				}
				if ( !$bool ) {
					setTimeout( function() {
						SPB.animatedHeadline.hideWord( $word );
					}, animationDelay)
				}
			}
		},
		takeNext: function ( $word ) {
			return ( !$word.is(':last-child') ) ? $word.next() : $word.parent().children().eq(0);
		},
		takePrev: function ( $word ) {
			return ( !$word.is(':first-child') ) ? $word.prev() : $word.parent().children().last();
		},
		switchWord: function ( $oldWord, $newWord ) {
			$oldWord.removeClass('is-visible').addClass('is-hidden');
			$newWord.removeClass('is-hidden').addClass('is-visible');
		}
	};


	SPB.onReady = {
		init: function() {

		}
	};

	SPB.onLoad = {
		init: function() {
			if ( jQuery('.spb-animated-headline').length > 0 ) {
				SPB.animatedHeadline.init();
			}
		}
	};

	jQuery(document).ready(SPB.onReady.init);
	jQuery(window).load(SPB.onLoad.init);

})(jQuery);