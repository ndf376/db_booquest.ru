== CHANGE LOG == 

= Version 1.0.9 =
* Added blog options

= Version 1.0.8 =
* Fixed home page sections content HTML issue

= Version 1.0.2 =
* Transfer data from theme options to customizer
* Fixed blog template sidebar issue
* Fixed blog pagingnav issue
* Fixed blog Excerpt Length issue

= Version 1.0.1 =
* Added bbpress style
* Fixed header padding issue
* Fixed page right sidebar issue
* Fixed top bar info color issue
* Fixed sticky header background color issue