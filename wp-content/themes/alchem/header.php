<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
 
<?php wp_head(); ?>
</head>
<?php
 global  $alchem_page_meta,$post,$alchem_banner_position,$alchem_banner_type,$alchem_options;
 
  $detect                      = new Mobile_Detect;
  $display_top_bar             = alchem_option('display_top_bar');
  $header_background_parallax  = alchem_option('header_background_parallax');
  $header_top_padding          = alchem_option('header_top_padding');
  $header_bottom_padding       = alchem_option('header_bottom_padding');
  $header_background_parallax  = $header_background_parallax=="yes"?"parallax-scrolling":"";
  $top_bar_left_content        = alchem_option('top_bar_left_content');
  $top_bar_right_content       = alchem_option('top_bar_right_content');
  
  //sticky
  $enable_sticky_header         = alchem_option('enable_sticky_header');
  $enable_sticky_header_tablets = alchem_option('enable_sticky_header_tablets');
  $enable_sticky_header_mobiles = alchem_option('enable_sticky_header_mobiles');
  $logo_position                = alchem_option('logo_position','left');
  $header_overlay               = alchem_option('header_overlay');

  $overlay = '';
  if( $header_overlay == 'yes')
  $overlay = 'overlay';
			  
 $layout     = esc_attr(alchem_option('layout','wide'));
 $full_width = isset($alchem_page_meta['full_width'])?$alchem_page_meta['full_width']:'no';
 $wrapper    = '';
 $body_class = '';
 if( $layout == 'boxed' )
 $wrapper    = ' wrapper-boxed container ';
 
 if( $full_width == 'yes' )
 $body_class     = 'page-full-width';
 
 if(isset($alchem_page_meta['nav_menu']) && $alchem_page_meta['nav_menu'] !='')
 $theme_location = $alchem_page_meta['nav_menu'];
 else
 $theme_location = 'primary'; 
 
 $header_position = isset($alchem_page_meta['header_position'])?$alchem_page_meta['header_position']:'top';
 switch( $header_position ){
	 case "top":
	 break;
	 case "left":
	 $body_class   .= ' side-header';
	 $wrapper       = '';
	 $logo_position = 'center';
	 $overlay = '';
	 break;
	 case "right":
	 $body_class   .= ' side-header side-header-right';
	 $wrapper       = '';
	 $logo_position = 'center';
	 $overlay       = '';
	 break;
	 } 
	 
// slider
 $alchem_banner_type     = isset($alchem_page_meta['slider_banner'])?$alchem_page_meta['slider_banner']:'0';
 $alchem_banner_position = isset($alchem_page_meta['banner_position'])?$alchem_page_meta['banner_position']:'1';

 if( $alchem_banner_type != '0' && $alchem_banner_type != '' ):
 if( $alchem_banner_position == '2'):
 $body_class   .= ' slider-above-header';
 else:
 $body_class   .= ' slider-below-header';
 endif;
 endif;
 $header_image = get_header_image();
?>
<body <?php body_class($body_class); ?>>
    <div class="wrapper <?php echo $wrapper;?>">   
    <div class="top-wrap">
    
    <?php if( $header_image ):?>
        <img src="<?php echo esc_url($header_image); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
         <?php endif;?>
         
    <?php if( $alchem_banner_position == '2'):
	           alchem_get_page_slider( $alchem_banner_type );
			   endif;
	?>
        <header class="header-wrap <?php echo $overlay; ?> logo-<?php echo $logo_position;?>">
        <?php if( $display_top_bar == 'yes' ):?>
            <div class="top-bar">
                <div class="container">
                    <div class="top-bar-left">
                          <?php  alchem_get_topbar_content( $top_bar_left_content );?>
                    </div>
                    <div class="top-bar-right">
                    <?php alchem_get_topbar_content( $top_bar_right_content );?>
                    </div>
                </div>
				<div class="clear"></div>
            </div>
            <?php endif;?>
             <?php 
					       $logo        = alchem_option('default_logo','');
					       $logo_retina = alchem_option('default_logo_retina');
						   $logo        = ( $logo == '' ) ? $logo_retina : $logo;
						   
						   $sticky_logo        = alchem_option('sticky_logo','');
						   $sticky_logo_retina = alchem_option('sticky_logo_retina');
						   $sticky_logo        = ( $sticky_logo == '' ) ? $sticky_logo_retina : $sticky_logo;
						   
					?>
            
            <div class="main-header <?php echo $header_background_parallax; ?>">
                <div class="container">
                    <div class="logo-box">
                  <a href="<?php echo esc_url(home_url('/')); ?>">
                    <?php if( $logo ):?>
                        <img class="site-logo normal_logo" alt="<?php bloginfo('name'); ?>" src="<?php echo esc_url($logo); ?>" />
                     <?php endif;?>
                       <?php
					if( $logo_retina ):
					$pixels ="";
					if(is_numeric(alchem_option('retina_logo_width')) && is_numeric(alchem_option('retina_logo_height'))):
					$pixels ="px";
					endif; ?>
					<img src="<?php echo esc_url($logo_retina); ?>" alt="<?php bloginfo('name'); ?>" style="width:<?php echo absint(alchem_option('retina_logo_width')).$pixels; ?>;max-height:<?php echo absint(alchem_option('retina_logo_height')).$pixels; ?>; height: auto !important" class="site-logo retina_logo" />
					<?php endif; ?>
                     </a>
                        <div class="name-box">
                            <a href="<?php echo esc_url(home_url('/')); ?>"><h1 class="site-name"><?php bloginfo('name'); ?></h1></a>
                            <span class="site-tagline"><?php bloginfo('description'); ?></span>
                        </div>
                    </div>
                    <button class="site-nav-toggle">
                        <span class="sr-only"><?php _e( 'Toggle navigation', 'alchem' );?></span>
                        <i class="fa fa-bars fa-2x"></i>
                    </button>
                    <nav class="site-nav" role="navigation">
                    <?php 
					 wp_nav_menu(array('theme_location'=>$theme_location,'depth'=>0,'fallback_cb' =>false,'container'=>'','container_class'=>'main-menu','menu_id'=>'menu-main','menu_class'=>'main-nav','link_before' => '<span>', 'link_after' => '</span>','items_wrap'=> '<ul id="%1$s" class="%2$s">%3$s</ul>'));
					?>
                    </nav>
                </div>
            </div>
            <?php if( $enable_sticky_header == 'yes' && $header_position !='left' && $header_position !='right' ):?>
            <?php if( !$detect->isTablet() || ( $detect->isTablet() && $enable_sticky_header_tablets == 'yes' ) || ( $detect->isMobile() && !$detect->isTablet() && $enable_sticky_header_mobiles == 'yes' )  ):?>
           <!-- sticky header -->
           <div class="fxd-header logo-<?php echo esc_attr($logo_position);?>">
                <div class="container">
                    <div class="logo-box text-left">
                        <a href="<?php echo esc_url(home_url('/')); ?>">
                    
                    <?php if( $sticky_logo ):?>
                        <img class="site-logo normal_logo" alt="<?php bloginfo('name'); ?>" src="<?php echo esc_url($sticky_logo); ?>" />
                     <?php endif;?>
                     
                       <?php
					if( $sticky_logo_retina ):
					$pixels ="";
					if( is_numeric(alchem_option('sticky_logo_width_for_retina_logo')) && is_numeric(alchem_option('sticky_logo_height_for_retina_logo')) ):
					$pixels ="px";
					endif; ?>
					<img src="<?php echo esc_url($sticky_logo_retina); ?>" alt="<?php bloginfo('name'); ?>" style="width:<?php echo absint(alchem_option('sticky_logo_width_for_retina_logo')).$pixels; ?>;max-height:<?php echo absint(alchem_option('sticky_logo_height_for_retina_logo')).$pixels; ?>; height: auto !important" class="site-logo retina_logo" />
					<?php endif; ?>
                     </a>
                        <div class="name-box">
                            <a href="<?php echo esc_url(home_url('/')); ?>"><h1 class="site-name"><?php bloginfo('name'); ?></h1></a>
                            <span class="site-tagline"><?php bloginfo('description'); ?></span>
                        </div>
                    </div>
                    <button class="site-nav-toggle">
                        <span class="sr-only"><?php _e( 'Toggle navigation', 'alchem' );?></span>
                        <i class="fa fa-bars fa-2x"></i>
                    </button>
                    <nav class="site-nav" role="navigation">
                        <?php 
					 wp_nav_menu(array('theme_location'=>$theme_location,'depth'=>0,'fallback_cb' =>false,'container'=>'','container_class'=>'main-menu','menu_id'=>'menu-main','menu_class'=>'main-nav','link_before' => '<span>', 'link_after' => '</span>','items_wrap'=> '<ul id="%1$s" class="%2$s">%3$s</ul>'));
					?>
                    </nav>
                </div>
            </div>
            <?php endif;?>
             <?php endif; ?>            
            <div class="clear"></div>
        </header>
        
         <?php if( $alchem_banner_position == '1'):
                alchem_get_page_slider( $alchem_banner_type );
			   endif;
	?>
	</div>