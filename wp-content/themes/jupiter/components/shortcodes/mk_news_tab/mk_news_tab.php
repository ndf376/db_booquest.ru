<?php
	
global $wp_version;

$path =  pathinfo( __FILE__ )['dirname'];

include( $path . '/config.php' );
require_once(THEME_FUNCTIONS . "/bfi_cropping.php");

$html = file_get_contents( $path . '/template.php' );
$html = phpQuery::newDocument( $html );

$id = uniqid();



$news_widget_title = pq( '.mk-fancy-title' );
if ( !empty( $widget_title ) ) {
	$news_widget_title->find( 'span' )
			->html( $widget_title );
} else {
	$news_widget_title->remove();
}

pq( '.mk-news-tab-title' )->html( $tab_title );


$query = array(
	'post_type'=>'news',
	'posts_per_page' => 3,
	'offset' => 0
);

$cat_terms = array();
$cat_terms = get_terms( 'news_category', 'hide_empty=1' );

// Get blueprints to populate and remove them
$tabs = pq( '.mk-tabs-tabs' );
$tab = $tabs->find( 'li' )
			->remove();

$panes = pq( '.mk-tabs-panes' );
$pane = $panes->find( '.mk-tabs-pane' )
			->remove();

$tab_count = 0;
foreach ( $cat_terms as $cat_term ) {
	// Populate tabs
	$new_tab = $tab->clone();
	$new_tab->find( 'a' )
			->html( $cat_term->name );
	$new_tab->appendTo( $tabs );

	$new_pane = $pane->clone()
			->appendTo( $panes );

	$new_pane->find( '.title-mobile ')->html( $cat_term->name );

	if( $tab_count == 0 ) {
		$new_tab->addClass( 'is-active' );
		$new_pane->addClass( 'is-active' );
	}


	$article_right = $new_pane->find( '.right-side')
			->remove();


	$query['tax_query'] = array(
		array(
			'taxonomy' => 'news_category',
			'field' => 'slug',
			'terms' => $cat_term->slug
		)
	);

	$newquery = new WP_Query( $query );
	$article_count = 0;

	if ( $newquery->have_posts() ) {
		while ( $newquery->have_posts() ) {
			$newquery->the_post();			

			$terms = get_the_terms( get_the_id(), 'news_category' );
			$terms_slug = array();
			$terms_name = array();

			if ( is_array( $terms ) ) {
				foreach ( $terms as $term ) {
					$terms_slug[] = $term->slug;
					$terms_name[] = $term->name;
				}
			}

			if( $article_count == 0 ) {
				$article = $new_pane->find( '.left-side');

				$thumbnail = $article->find( '.news-tab-thumb' );

				if ( has_post_thumbnail() ) {
					$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
					$image_src = bfi_thumb( $image_src_array[ 0 ], array('width' => 500, 'height' => 340));
				
					$thumbnail->attr( 'href', get_permalink() )
							->find( 'img' )
							->attr( 'src', $image_src )
							->attr( 'alt', get_the_title() )
							->attr( 'title', get_the_title() );
				} else {
					$thumbnail->remove();
				}
				
			} else {
				$article = $article_right->clone()
						->appendTo( $new_pane );
			}

			// For all
			$article->find( '.the-title a' )
					->attr( 'href', get_permalink() )
					->html( get_the_title() );

			$article->find( '.the-excerpt' )
					->html( get_the_excerpt() );

			$article->find( '.new-tab-readmore' )
					->attr( 'href', get_permalink() )
					->find( 'span' )
					->html( __( 'Read More', 'mk_framework' ) );


			$article_count++;
		}

		if( $article_count == 0 ) {
			$new_pane->find( '.right-side')->remove();
		}

		wp_reset_query();
	}
	$tab_count++;
}


/**
 * Custom CSS Output
 * ==================================================================================*/


pq( '.mk-news-tab' )->addClass( 'mobile-'.$responsive );
if( !empty( $el_class ) ) {
	pq( '.mk-news-tab' )->addClass( $el_class );
}

print $html;

?>