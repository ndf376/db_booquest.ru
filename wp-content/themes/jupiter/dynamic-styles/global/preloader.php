<?php

global $mk_options;

$preloader_check = '';
$post_id = global_get_post_id();
        
$singular_preloader = ($post_id) ? get_post_meta($post_id, 'page_preloader', true) : '';

if ($singular_preloader == 'true') {
    $preloader_check = 'enabled';
} 
else {
    if ($mk_options['preloader'] == 'true') {
        $preloader_check = 'enabled';
    }
}

if ($preloader_check != 'enabled') return false;


$loaderStyle = $mk_options['preloader_animation'];
if($loaderStyle == "ball_pulse"){
	Mk_Static_Files::addGlobalStyle('
		.page-preloader .ball-pulse > div {
			background-color: '.$mk_options['preloader_icon_color'].';
		}
	');	
}else if($loaderStyle == "ball_clip_rotate_pulse") {
	Mk_Static_Files::addGlobalStyle('
		.page-preloader .ball-clip-rotate-pulse > div:first-child {
			background-color: '.$mk_options['preloader_icon_color'].';
		}
		.page-preloader .ball-clip-rotate-pulse > div:last-child {
			border-color: '.$mk_options['preloader_icon_color'].' transparent '.$mk_options['preloader_icon_color'].' transparent;
		}
	');
}else if($loaderStyle == "square_spin") {
	Mk_Static_Files::addGlobalStyle('
		.page-preloader .square-spin > div {
			background-color: '.$mk_options['preloader_icon_color'].';
		}
	');
}else if($loaderStyle == "cube_transition") {
	Mk_Static_Files::addGlobalStyle('
		.page-preloader .cube-transition > div {
			background-color: '.$mk_options['preloader_icon_color'].';
		}
	');
}else if($loaderStyle == "ball_scale") {
	Mk_Static_Files::addGlobalStyle('
		.page-preloader .ball-scale > div {
			background-color: '.$mk_options['preloader_icon_color'].';
		}
	');
}else if($loaderStyle == "line_scale") {
	Mk_Static_Files::addGlobalStyle('
		.page-preloader .line-scale > div {
			background-color: '.$mk_options['preloader_icon_color'].';
		}
	');
}else if($loaderStyle == "ball_scale_multiple") {
	Mk_Static_Files::addGlobalStyle('
		.page-preloader .ball-scale-multiple > div {
			background-color: '.$mk_options['preloader_icon_color'].';
		}
	');
}else if($loaderStyle == "ball_pulse_sync") {
	Mk_Static_Files::addGlobalStyle('
		.page-preloader .ball-pulse-sync > div {
			background-color: '.$mk_options['preloader_icon_color'].';
		}
	');
}else if($loaderStyle == "transparent_circle") {
	Mk_Static_Files::addGlobalStyle('
		.page-preloader .transparent-circle {
			border-top-color: '.mk_hex2rgba($mk_options['preloader_icon_color'], 0.2).';
			border-right-color: '.mk_hex2rgba($mk_options['preloader_icon_color'], 0.2).';
			border-bottom-color: '.mk_hex2rgba($mk_options['preloader_icon_color'], 0.2).';
			border-left-color: '.$mk_options['preloader_icon_color'].';
		}
	');
}else if($loaderStyle == "ball_spin_fade_loader") {
	Mk_Static_Files::addGlobalStyle('
		.page-preloader .ball-spin-fade-loader > div {
			background-color: '.$mk_options['preloader_icon_color'].'
		}
	');
}
